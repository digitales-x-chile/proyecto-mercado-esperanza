<div id="me-map" style="width: 100%; height: 500px"></div>

<script type="text/javascript">
    var cloudmade = new CM.Tiles.CloudMade.Web({key: 'E282E0EA16754AFB98173BF9A17CF874'});
    var map = new CM.Map('me-map', cloudmade);
    map.setCenter(new CM.LatLng(51.514, -0.137), 15);

    var markers = [];
    <?php
	$y1 = -86;
	$x1 = -180;
	$y2 = 86;
	$x2 = 180;
	foreach($vars['users'] as $user) {
	    $lat = $user->getLatitude();
	    $lng = $user->getLongitude();
	    $y1 = max(array($lat, $y1));
	    $x1 = max(array($lng, $x1));
	    $y2 = min(array($lat, $y2));
	    $x2 = min(array($lng, $x2));
    ?>
	markers.push(new CM.Marker(new CM.LatLng(<?php echo "{$user->getLatitude()}, {$user->getLongitude()}"; ?>)));
	var mapaMarker = new CM.Marker(new CM.LatLng(<?php echo "{$user->getLatitude()}, {$user->getLongitude()}"; ?>))
	map.addOverlay(mapaMarker);
    <?php }

	$y1 = $y1 + 0.002;
	$x1 = $x1 + 0.002;
	$y2 = $y2 - 0.002;
	$x2 = $x2 - 0.002;

    ?>

    //var clusterer = new CM.MarkerClusterer(map, {clusterRadius: 70});
    //clusterer.addMarkers(markers);

    var southWest = new CM.LatLng(<?php echo "$y2, $x2"; ?>),
	northEast = new CM.LatLng(<?php echo "$y1, $x1"; ?>);

    map.zoomToBounds(new CM.LatLngBounds(southWest, northEast));

</script>