<?php

function me_map_init() {

    register_page_handler('map', 'me_map_page_handler');

    elgg_extend_view('metatags', 'me_map/load_js');

}

function me_map_page_handler($path) {

    $users = elgg_get_entities(array(
	'type' => 'user'
    ));

    $body = elgg_view('me_map/map', array('users' => $users));

    page_draw(elgg_echo('me_map:map'), $body);

}

register_elgg_event_handler('init', 'system', 'me_map_init');
