$(document).ready(function(e)
{
    $('.fancy-link').fancybox({
        'padding': 0,
        'margin':0,
        'titleShow': false,
        'showCloseButton': false
    });

    $('.blank').attr('target', '_blank');
});